# People Against Prisons Aotearoa Warrant Canary

This repository serves as a warrant canary for People Against Prisons Aotearoa,
and is to confirm that we have not been served with any secret warrants or
subpoenas.

We will publish a new canary on the 20th day of every month.

[The current canary (as of 2025-02-20) is available here.][canary]

[canary]: ./canaries/canary-2025-02-20.md

## GPG keys

All canary updates will be GPG signed with one of the keys available in the
`keys/` subfolder of this repository.

The current key is key ID 10E4 3592 5AAA 4E5D 2B82  2C3F 6753 CAD3 2B06 C65D,
found in the file [`canary-key-2023.pub`](./keys/canary-key-2023.pub).

All commits adding new keys will be signed by a key that previously existed in
the repository. The only exception to this is the initial commit to this
repository, which is signed with the personal key of Alice Jenkinson, key ID
49C3 A999 1B16 2355 BE29  4BC0 7B4D 0DB5 202C 6EED`.

