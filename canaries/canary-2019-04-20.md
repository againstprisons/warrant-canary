# Warrant Canary, 2019-04-20

As of today, the 20th of April 2019:

* We have not placed any backdoors into our services, and we have not complied
  with any requests to do so.
* We have not received a secret warrant or subpoena from any government agency.

Warrant canary statements will be updated on the 20th day of every month.
