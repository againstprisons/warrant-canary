# Warrant Canary, 2024-09-20

As of today, the 20th of September 2024:

* We have not placed any backdoors into our services, and we have not complied
  with any requests to do so.
* We have not received a secret warrant or subpoena from any government agency.

Warrant canary statements will be updated on the 20th day of every month. This canary was posted a day late due to operator error.
